# 1. Definition of the Project

## Introduction

The classification of pixels follows iterative steps. Each step generates outputs that will be used a inputs in the next step. Directory management is therefore critical even for a simple study. If you want to make the tool available to multiple users, perform multiple studies for the same operator, compare the analysis of multiple operators, or multiple analyses of the same operator, directory management is even more important. This is why this module has been developed.

Defining a project then consists in defining all the directory locations. Version 5.3 of Fiber allows to set this set of directories in 3 ways :
* by default, when the software is used for a single study and a single user.  In this case the different directories are defined by default by FIBER and created if they do not yet exist. The default parent directory is next to the directory containing the FIBER programs. A simple validation is enough to confirm this location. However, the location of the default directory can be changed or reset to its original value.
* pre-formatted : the operator defines a unique working directory using three identifiers : the study name, its personal identifier (trigram) and its reading number. FIBER is then in charge of creating a tree of sub-directories below this working directory. This simplified procedure makes it possible to use Fiber in multi-user or multi-studies mode. 
* free : the operator can modify each of the sub-directories corresponding to the different calculation steps. This more complicated use is to be reserved for variations from a main analysis. For example, a second analysis of the data can be performed by keeping the original data directories but changing the location of the analysis results...

## Panel

The project definition window is divided into three parts. 

* **|A>Upper part**: study identifiers.
* **|B>Middle part** : details of each sub-folder. 
* **|C> Lower part**: action buttons.

The three identifiers  in |A> make it easy to manage multiple readings (or analyses), inter and intra users.

![](.\figure2.png)

### Features

1. Open an existing analysis (green) which is identified by a study name, your identifier, a reading number. If your folders are not public you can associate them with a password *. Then press [open]. 

    > Beware, the security system associated with the password is a lightweight system that prevents accidental errors, not malicious intents. Forgotten passwords can be retrieved from the environment directory (ENV). The memo list allows to check all the current analyses. However, to avoid click errors, the identifiers must be entered using the keyboard.
    
2. Create / Open the default workspace. All working directories are positioned in the directory that contains the FIBER program directory.

    <img src="./figure3.png" style="zoom: 67%;" />

    > Validating the directory validates the use of the default location. If this default location is not suitable, simply change the directory. The new directory will be the new default directory. To restore the original default directory, click under the [Defautl] button at the bottom of the first figure.

3. Define a formatted workspace elsewhere than in the default directory. Provide the identifiers of the new analysis and the parent directory in which all working directories will be created.

    > A verification system prevents the redefinition of an existing analysis. To modify an existing study, you must first open it and then edit it.

    <img src="./figure4.png" style="zoom:80%;" />

4. Define each directory in detail, if required. All directories used in Fiber are listed in |B>. The user can choose his own directories. In this way, it is possible to organize multiple analysis backups (several learning operations performed on the same data, calculations performed on different regions of interest, several users). 

    > The location can be typed by hand, copied and pasted, or selected by the operating system's folder manager (click on [find...].)

5. If the operator has defined the identifiers of a new study, or if a study has been opened and then modified, it can be saved by pressing the [Save] button. If the directories displayed in |B> do not exist in the computer, they are created.

6. Validation of the analysis workspace for the further processing of FIBER: [OK].

    > To work on an ongoing study, only steps 1 (opening) and 6 (validation) are performed.

​	[return to the main page](.\FIBER.html)