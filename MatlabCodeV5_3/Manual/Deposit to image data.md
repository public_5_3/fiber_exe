

# 2. Deposit of image data

The first button of the **Data panel** allows you to select images from a data directory. These data are retrieved in the FIBER analysis study data directory (see [1] in the graph on the left side of the image below).

The second button of the panel allows to select sample images to copy them into the learning data directory (see [2] in the graph on the left of the image below).

Alternatively it is possible to point the study data directory directly to a directory already containing data (see the graph on the right of the figure below).

<img src=".\figure5.png" style="zoom:80%;" />

The image data are assumed to be TIFF images of sufficiently high quality (luminous homogeneity, homogeneity of the staining quality, low or no compression). Most of the information useful for classification is assumed to come from the pixels intensities and not from the spatial structuring of such intensities.

> The module for importing data from manufacturer formats is not integrated yet in version 5.3
>
> Importing in tiff format must be done with care. Poor data will provide poor results.
>
> Click on the images which will be copied on the study and learning data.

[return to the main page](.\FIBER.html)

