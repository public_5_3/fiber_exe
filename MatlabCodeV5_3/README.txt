Here you are the open source Matlab code of FIBER 5.3 developed with the 2021a version of Matlab.

To launch, write FIBER in the command line or install FIBER as an app in the menu.
