function im=cut(im,hroi)
% crop an image from a roi object of from a derived rectangle
% FOR FIBER 5.2 D. Balvay - 17 dec 2020 - PARCC U970 Inserm/Univ. Paris, France
if isobject(hroi) % hypothese objet images.roi
    if isprop(hroi,'Vertices')
        pos=hroi.Vertices;
    else
        pos=hroi.Position;
    end
    mini=ceil(min(pos,[],1));
    maxi=floor(max(pos,[],1));

else    % hypothesis
    cadre=hroi; % a rectangle
    mini=ceil(cadre(1,:));
    maxi=floor(cadre(2,:));
end
mini=max([mini;1 1]);
[nx,ny,~]=size(im);
maxi=min([maxi;ny nx]);
if ndims(im)==2
    try
       im=im(mini(2):maxi(2),mini(1):maxi(1));
    catch
        disp('debug');
    end
else
    try
       im=im(mini(2):maxi(2),mini(1):maxi(1),:);
    catch
        disp('debug');
    end
    
end