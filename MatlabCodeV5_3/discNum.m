function sn = discNum(filename,fixedValue)
% get an Id of the computer and write it in a 'filename' if asked.
% A part of the true disc Id is removed to avoid a trace of "personal data" in
% files if copied.
% IN : 
%   filename (option) mat filename where to save the Id
%   fixedValue : Id fixed and not get from the computer

if nargin<2
    cmd = 'wmic diskdrive get SerialNumber';
    [~, result] = system(cmd);
    %// Extract first hard disk serial number
    fields = textscan( result, '%s', 'Delimiter', '\n' );
    fields = strtrim(fields{1});
    sn = fields{2}; % serial number
        % keep half number 
    sn = split(sn,'_');
    if numel(sn)>1
        sn = join(sn(1:2),'_');
        sn = sn {1};
    else
        sn = sn{1};
        sn = sn(1:5);
    end
else
    sn = fixedValue;
end



% write file
if nargin>0 && ~isempty(filename)
    save(filename,'sn');
end