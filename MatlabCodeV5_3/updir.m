function repout=updir(repin)

tags=split(repin,filesep);
res=join(tags(1:end-1),filesep);
repout=res{1};