# FIBER_EXE


**FIBER** is a graphical interface allowing to segment batches of histological images (tiff) from an initial learning phase.
it provides segmented maps by tissue class and a pixel count of each class.

This is a public repository of the executable version of FIBER 3.5. The open source code, in Matlab  format will be deposited after publication of the first paper describing the software.

Fiber was designed for specific markings only ( color carries information)

## steps of the analysis

1) Settings : Define/Select of the workspace of the analysis (it can work on multiple studies)
2) Learning : Define/select labels and associate pixels of the leaning images to such labels (or classes)
3) Localisation (option) : Define one or several regions of interest in the images
4) Production : Learning data are used to fit the classifier wich is applied on pixels of all the images of the study
5) Visual Control : Control visually the segmentation on a sample of images and fill (option) a form according to your comments
6) Summary : Provide a tab with counting and quality checking of the all process
The source code, in Matlab(r)  format, will be deposited after publication of the main paper on the software.

for more details clic [here] (.\UserGuide\FIBER.html)





* [CompanionData] contains images to test FIBER even if you don't have your data at hand: fiels and slices.
* [FIBER5_3]
  * [ENV] : contains a file indicating the default working directory.
  * [for-redistribution] : FIBER installers, including and not the Matlab(r) runtime.
  * [Manual] : FIBER Manual in html (main : FIBER.html). Available directly or via the Fiber menu.
  * DefLabel : Labels displayed by default
  * DefLoc : Default location

